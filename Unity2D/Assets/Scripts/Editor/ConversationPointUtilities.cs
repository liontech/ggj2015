﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public static class ConversationPointUtilities {

    [MenuItem("Assets/Create/Conversation Point")]
    public static void CreateAsset() {
        ScriptableObjectUtility.CreateAsset<ConversationPoint>();
    }

}
