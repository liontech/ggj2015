﻿using UnityEngine;
using System.Collections;

public class NPC : MonoBehaviour {

    private Conversation myConversation;
    private Character myCharacter;

    private GameObject conversationOpenerWidget;

    private void OnPlayerEnter() {
        if (myConversation != null) {
            conversationOpenerWidget = UIHelper.Instantiate("Prefabs/Conversation/ConversationOpener", UILayer.Conversation, transform);
        }
    }

    private void OnPlayerExit() {
        if (conversationOpenerWidget != null) {
            GameObject.Destroy(conversationOpenerWidget);
        }
    }

    private void Awake() {
        myCharacter = GetComponent<Character>();
        myConversation = GetComponent<Conversation>();
        GetComponent<PlayerTrigger>().OnPlayerEnter += OnPlayerEnter;
        GetComponent<PlayerTrigger>().OnPlayerExit += OnPlayerExit;
    }

    private void Update() {
        HandleConversationOpening();
    }

    private void HandleConversationOpening() {
        if (conversationOpenerWidget == null) { return; }
        if (myConversation.InConversation) { return; }
        if (InputManager.GetInputDown(InputKey.Select)) {
            myConversation.StartConversation();
            GameObject.Destroy(conversationOpenerWidget);
        }
    }

}
