﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    public bool WalkEnabled = true;

    private Character myCharacter;

    private void Update() {
        UpdateWalking();
    }

    private void Awake() {
        myCharacter = GetComponent<Character>();
    }

    private void UpdateWalking() {
        if (!WalkEnabled) { return; }
        if (InputManager.GetInputHold(InputKey.WalkLeft)) {
            myCharacter.Move(Vector2.right * -1);
        }
        if (InputManager.GetInputHold(InputKey.WalkRight)) {
            myCharacter.Move(Vector2.right);
        }
        if (InputManager.GetInputHold(InputKey.WalkUp)) {
            myCharacter.Move(Vector2.up);
        }
        if (InputManager.GetInputHold(InputKey.WalkDown)) {
            myCharacter.Move(Vector2.up * -1);
        }
    }

}
