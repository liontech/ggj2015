﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerTrigger : MonoBehaviour {

    public Action OnPlayerEnter;
    public Action OnPlayerExit;

    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.collider.gameObject.layer != LayerMask.NameToLayer("Player")) { return; }
        if (OnPlayerEnter != null){
            OnPlayerEnter();
        }
    }

    private void OnCollisionExit2D(Collision2D collision) {
        if (collision.collider.gameObject.layer != LayerMask.NameToLayer("Player")) { return; }
        if (OnPlayerExit != null) {
            OnPlayerExit();
        }
    }

}
