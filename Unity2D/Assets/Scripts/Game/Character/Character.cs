﻿using UnityEngine;
using System.Collections;

public class Character : MonoBehaviour {

    [SerializeField] private float walkSpeed;

    private Rigidbody2D myRigidBody;

    public void Move(Vector2 direction) {
        myRigidBody.AddForce(direction * walkSpeed);
    }

    private void Awake() {
        myRigidBody = GetComponent<Rigidbody2D>();
    }

}
