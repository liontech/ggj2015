﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

public class ConversationPoint : ScriptableObject {

    public string Text;
    public List<ConversationAnswer> Answers;
    public bool Cancalable;

}
