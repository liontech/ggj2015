﻿using UnityEngine;
using System.Collections;

public class Conversation : MonoBehaviour {

    public bool InConversation { get { return pointWidget != null; } }

    [SerializeField] private ConversationPoint startPoint;

    private ConversationPointWidget pointWidget;

    public void StartConversation() {
        StopConversation();
        InstantiateConversationPointWidget(startPoint);
    }

    public void StopConversation() {
        if (pointWidget == null) { return; }
        pointWidget.Close();
    }

    private void OnPointAnswered(ConversationPointWidget pointWidget, ConversationAnswer answer) {
        pointWidget.Close();
        if (answer != null && answer.Target != null) {
            InstantiateConversationPointWidget(answer.Target);
        }
    }

    private void InstantiateConversationPointWidget(ConversationPoint point) {
        pointWidget = UIHelper.Instantiate("Prefabs/Conversation/ConversationPoint", UILayer.Conversation, transform).GetComponent<ConversationPointWidget>();
        pointWidget.Initialize(point);
        pointWidget.OnAnswer += OnPointAnswered;
    }

}
