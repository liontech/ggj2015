﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class ConversationAnswer {

    public string Text;
    public ConversationPoint Target;

}
