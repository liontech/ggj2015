﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class ConversationAnswerWidget : MonoBehaviour {

    public ConversationAnswer Answer { get; private set; }

    public void Initialize(ConversationAnswer answer) {
        Answer = answer;
        GetComponent<Text>().text = answer.Text;
    }

    public void SetFocused(bool selected) {
        GetComponent<Text>().fontStyle = selected ? FontStyle.Bold : FontStyle.Normal;
    }

}
