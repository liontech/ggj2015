﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class ConversationPointWidget : MonoBehaviour {

    public Action<ConversationPointWidget, ConversationAnswer> OnAnswer;

    private ConversationPoint point;
    private List<ConversationAnswerWidget> answerWidgets = new List<ConversationAnswerWidget>();
    private int focusedAnswerIndex = 0;

    public void Initialize(ConversationPoint point) {
        this.point = point;

        gameObject.FindChildByName<Text>("r_ConversationText").text = point.Text;
        
        GameObject answersGrid = gameObject.FindChildByName("r_AnswersGrid");
        GameObject answerTemplate = gameObject.FindChildByName("t_Answer");
        foreach (ConversationAnswer answer in point.Answers) {
            ConversationAnswerWidget answerWidget = (GameObject.Instantiate(answerTemplate) as GameObject).GetComponent<ConversationAnswerWidget>();
            answerWidget.Initialize(answer);
            answerWidget.transform.SetParent(answersGrid.transform);
            answerWidgets.Add(answerWidget);
        }
        answerTemplate.SetActive(false);
        focusedAnswerIndex = point.Answers.Count - 1;
        UpdatefocusedAnswer();

        FindObjectOfType<Player>().WalkEnabled = false;
    }

    public void Close() {
        FindObjectOfType<Player>().WalkEnabled = true;
        GameObject.Destroy(gameObject);
    }

    private void Update() {
        if (InputManager.GetInputDown(InputKey.TabRight)) {
            focusedAnswerIndex--;
            UpdatefocusedAnswer();
        }

        if (InputManager.GetInputDown(InputKey.TabLeft)) {
            focusedAnswerIndex++;
            UpdatefocusedAnswer();
        }

        if (InputManager.GetInputDown(InputKey.Select)) {
            if (OnAnswer != null) {
                if (answerWidgets.Count == 0) {
                    OnAnswer(this, null);
                } else {
                    OnAnswer(this, answerWidgets[focusedAnswerIndex].Answer);
                }
            }
        }
    }

    private void OnDestroy() {
        OnAnswer = null;
    }

    private void UpdatefocusedAnswer() {
        if (focusedAnswerIndex > point.Answers.Count - 1) {
            focusedAnswerIndex = 0;
        }
        if (focusedAnswerIndex < 0) {
            focusedAnswerIndex = point.Answers.Count - 1;
        }
        for (int i = 0; i < answerWidgets.Count; i++) {
            answerWidgets[i].SetFocused(focusedAnswerIndex == i);
        }
    }

}
