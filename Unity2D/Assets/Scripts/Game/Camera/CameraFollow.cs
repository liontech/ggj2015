﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

	[SerializeField] private Transform target;
    [SerializeField] private float marginHor;
    [SerializeField] private float marginVert;

    private Camera myCamera;

    private void Awake() {
        myCamera = GetComponent<Camera>();
    }

    private void Update() {
        if (target == null) { return; }

        Vector2 boundsMin = myCamera.BoundsMin();
        Vector2 boundsMax = myCamera.BoundsMax();

        if (target.position.x > boundsMax.x - marginHor) {
            myCamera.transform.position += Vector3.right * (target.position.x - (boundsMax.x - marginHor));
        } else if (target.position.x < boundsMin.x + marginHor) {
            myCamera.transform.position -= Vector3.right * ((boundsMin.x + marginHor) - target.position.x);
        }

        if (target.position.y > boundsMax.y - marginVert) {
            myCamera.transform.position += Vector3.up * (target.position.y - (boundsMax.y - marginVert));
        } else if (target.position.y < boundsMin.y + marginVert) {
            myCamera.transform.position -= Vector3.up * ((boundsMin.y + marginVert) - target.position.y);
        }
    }

}
