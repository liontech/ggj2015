﻿using UnityEngine;
using System.Collections;

public enum UILayer {
    Conversation
}

public static class UIHelper {

    public static GameObject Instantiate(string resourcePath, UILayer layer) {
        return Instantiate(resourcePath, layer, null);
    }

    public static GameObject Instantiate(string resourcePath, UILayer layer, Transform uiAnchorTarget) {
        GameObject go = GameObject.Instantiate(Resources.Load(resourcePath)) as GameObject;
        go.transform.SetParent(GameObject.Find("r_" + layer.ToString() + "Layer").transform);
        if (uiAnchorTarget != null) {
            go.GetComponent<UIAnchor>().Target = uiAnchorTarget;
        }
        return go;
    }
	
}
