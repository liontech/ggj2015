﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class ListHelper {

    public static T GetRandomElement<T>(this List<T> list) {
        if (list.Count == 0) { return default(T); }
        return list[Random.Range(0, list.Count)];
    }

}
