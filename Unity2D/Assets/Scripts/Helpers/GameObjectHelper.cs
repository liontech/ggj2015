﻿using UnityEngine;
using System.Collections;

public static class GameObjectHelper {

    public static GameObject FindChildByName(this GameObject go, string name) {
        Transform transform = go.transform;
        foreach (Transform t in transform) {
            if (t.name == name) {
                return t.gameObject;
            }
            GameObject result = t.gameObject.FindChildByName(name);
            if (result != null) {
                return result;
            }
        }
        return null;
    }

    public static T FindChildByName<T>(this GameObject go, string name) where T : Component {
        Transform transform = go.transform;
        foreach (Transform t in transform) {
            if (t.name == name) {
                T component = t.GetComponent<T>();
                if (component != null) {
                    return component;
                }
            }
            T result = t.gameObject.FindChildByName<T>(name);
            if (result != null) {
                return result;
            }
        }
        return null;
    }

}
