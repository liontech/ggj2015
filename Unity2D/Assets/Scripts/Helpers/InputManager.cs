﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public enum InputKey {
    TabRight,
    TabLeft,
    Select,
    WalkUp,
    WalkDown,
    WalkRight,
    WalkLeft,
}

public static class InputManager {

    private static bool initialized;
    private static Dictionary<InputKey, List<KeyCode>> sortedBindings;

    public static bool GetInputDown(InputKey input) {
        Initialize();
        if (!sortedBindings.ContainsKey(input)) { return false; }
        foreach (KeyCode keyCode in sortedBindings[input]) {
            if (Input.GetKeyDown(keyCode)) {
                return true;
            }
        }
        return false;
    }

    public static bool GetInputHold(InputKey input) {
        Initialize();
        if (!sortedBindings.ContainsKey(input)) { return false; }
        foreach (KeyCode keyCode in sortedBindings[input]) {
            if (Input.GetKey(keyCode)) {
                return true;
            }
        }
        return false;
    }

    private static void Initialize() {
        if (initialized) { return; }
        initialized = true;

        sortedBindings = new Dictionary<InputKey, List<KeyCode>>();
        sortedBindings.Add(InputKey.TabLeft, new List<KeyCode>() { KeyCode.LeftArrow });
        sortedBindings.Add(InputKey.TabRight, new List<KeyCode>() { KeyCode.RightArrow });
        sortedBindings.Add(InputKey.Select, new List<KeyCode>() { KeyCode.Return });
        sortedBindings.Add(InputKey.WalkUp, new List<KeyCode>() { KeyCode.UpArrow });
        sortedBindings.Add(InputKey.WalkDown, new List<KeyCode>() { KeyCode.DownArrow });
        sortedBindings.Add(InputKey.WalkRight, new List<KeyCode>() { KeyCode.RightArrow });
        sortedBindings.Add(InputKey.WalkLeft, new List<KeyCode>() { KeyCode.LeftArrow });
    }

}
