﻿using UnityEngine;
using System.Collections;

public class ConversationTester : MonoBehaviour {

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Space)) {
            GetComponent<Conversation>().StartConversation();
        }
    }

}
