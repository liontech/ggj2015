﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class AreaDepthSprite : MonoBehaviour {

    private const float LERP_HEIGHT = 40.0f;
    private const float Z_OFFSET_MAX = 10.0f;

	private void Awake() {
        UpdateDepth();
	}
	
	
	private void Update() {
        UpdateDepth();
	}

    private void UpdateDepth() {
        float z = Mathf.Clamp((.5f * LERP_HEIGHT - transform.position.y) / LERP_HEIGHT * -Z_OFFSET_MAX, -Z_OFFSET_MAX, 0.0f);
        transform.position = new Vector3(transform.position.x, transform.position.y, z);
    }
}
