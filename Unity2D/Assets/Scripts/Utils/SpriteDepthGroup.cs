﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpriteDepthGroup : MonoBehaviour {

    private static Dictionary<int, List<SpriteDepthGroup>> layers = new Dictionary<int, List<SpriteDepthGroup>>();

    private const int DEPTH_ALLOCATION = 30;

    private void Awake() {
        SpriteRenderer first = GetComponentInChildren<SpriteRenderer>();
        if (first == null) {
            Debug.LogWarning("Can't find sprites in SpriteDepthGroup!", this);
            return;
        }

        int layer = first.sortingLayerID;
        if (!layers.ContainsKey(layer)) {
            layers.Add(layer, new List<SpriteDepthGroup>());
        }
        List<SpriteDepthGroup> depthGroups = layers[layer];

        int thisIndex = -1;
        for (int i = depthGroups.Count-1; i >= 0; i--) {
            if (depthGroups[i] == null) {
                if (thisIndex == -1) {
                    depthGroups[i] = this;
                    thisIndex = i;
                } else {
                    depthGroups.RemoveAt(i);
                }
            }
        }
        if (thisIndex == -1) {
            depthGroups.Add(this);
            thisIndex = depthGroups.Count - 1;
        }

        SpriteRenderer[] spriteRenderers = GetComponentsInChildren<SpriteRenderer>();
        foreach (SpriteRenderer spriteRenderer in spriteRenderers) {
            spriteRenderer.sortingOrder += thisIndex * DEPTH_ALLOCATION;
        }
    }

}
