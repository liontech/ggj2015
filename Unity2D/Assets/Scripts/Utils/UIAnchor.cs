﻿using UnityEngine;
using System.Collections;

public class UIAnchor : MonoBehaviour {

    public Transform Target {
        get {
            return target;
        }
        set {
            target = value;
            Update();
        }
    }

    [SerializeField] private Transform target;

    private RectTransform myRect;
    private RectTransform canvasTransform;

    private void Awake() {
        myRect = GetComponent<RectTransform>();
        canvasTransform = FindObjectOfType<MainCanvas>().GetComponent<RectTransform>();
    }

	private void Update () {
        Vector3 viewPos = Camera.main.WorldToViewportPoint(target.transform.position);

        Vector3 pos;
        pos.x = canvasTransform.rect.width * viewPos.x - canvasTransform.rect.width / 2f;
        pos.y = canvasTransform.rect.height * viewPos.y - canvasTransform.rect.height / 2f;
        pos.z = 0f;

        transform.localPosition = pos;
	}
}
