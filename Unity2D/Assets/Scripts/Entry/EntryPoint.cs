﻿using UnityEngine;
using System.Collections;

public class EntryPoint : MonoBehaviour {

    [SerializeField] private string startLevel;

    private void Start() {
        Application.LoadLevel(startLevel);
    }

}
