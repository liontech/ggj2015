﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class AudioManager : MonoBehaviour {

    [Serializable] 
    private class SoundEvent {
        public string Event = "";
        public AudioClip Clip = null;
        public float Volume = 1.0f;
    }

    [Serializable]
    private class Music {
        public string Name = "";
        public AudioClip Clip = null;
        public float Volume = 1.0f;
    }

    [Serializable]
    private class PlayingMusic {
        public string Name;
        public AudioSource Source;
        public float Volume;
    }

    private static AudioManager instance;
    private static Dictionary<string, SoundEvent> staticSoundEvents;
    private static Dictionary<string, Music> staticMusics;
    private static PlayingMusic playingMusic;
    private static Dictionary<string, AudioSource> fadingMusics;

    [SerializeField] private float musicFadeDuration = 1.0f;
    [SerializeField] private List<SoundEvent> soundEvents = new List<SoundEvent>();
    [SerializeField] private List<Music> musics = new List<Music>();

    public static void Play(string eventName) {
        if (!staticSoundEvents.ContainsKey(eventName)) {
            Debug.LogError("Can't play sound event '" + eventName + "' because it does not exist!");
        }
        SoundEvent e = staticSoundEvents[eventName];
        
        AudioSource.PlayClipAtPoint(e.Clip, Vector3.zero, e.Volume);
    }

    public static void PlayMusic(string musicName, bool stopCurrent = true) {
        if (!staticMusics.ContainsKey(musicName)) {
            Debug.LogError("Can't play music '" + musicName + "' because it does not exist!");
        }

        // Check if music is already playing
        if (playingMusic != null && playingMusic.Name == musicName) { return; }

        // If music is not playing, make sure it fades out
        if (playingMusic != null) {
            fadingMusics.Add(playingMusic.Name, playingMusic.Source);
        }

        AudioSource source = null;

        // Check if music source is fading out and has to be revived
        if (fadingMusics.ContainsKey(musicName)) {
            source = fadingMusics[musicName];
            fadingMusics.Remove(musicName);
        } else {
            GameObject go = new GameObject("Music");
            go.transform.SetParent(instance.transform);
            source = go.AddComponent<AudioSource>();
            source.loop = true;
            source.clip = staticMusics[musicName].Clip;
            source.Play();
        }

        playingMusic = new PlayingMusic() {
            Name = musicName,
            Source = source,
            Volume = staticMusics[musicName].Volume
        };
    }

    public static void StopMusic() {
        if (playingMusic == null) { return; }
        fadingMusics.Add(playingMusic.Name, playingMusic.Source);
        playingMusic = null;
    }

    private void Awake() {
        if (instance != null) {
            GameObject.Destroy(gameObject);
            return; 
        }
        instance = this;
        DontDestroyOnLoad(instance);

        staticSoundEvents = new Dictionary<string, SoundEvent>();
        staticMusics = new Dictionary<string, Music>();
        playingMusic = null;
        fadingMusics = new Dictionary<string, AudioSource>();

        foreach (SoundEvent e in soundEvents) { 
            staticSoundEvents.Add(e.Event, e); 
        }
        foreach (Music m in musics) { 
            staticMusics.Add(m.Name, m); 
        }
    }

    private void Update() {
        // Fade in playing music
        if (playingMusic != null) {
            if (playingMusic.Source.volume < playingMusic.Volume) {
                playingMusic.Source.volume += Time.deltaTime / musicFadeDuration;
                playingMusic.Source.volume = Mathf.Min(playingMusic.Volume, playingMusic.Source.volume);
            }
        }

        // Fade out the fading musics
        foreach (KeyValuePair<string, AudioSource> pair in fadingMusics) {
            pair.Value.volume -= Time.deltaTime / musicFadeDuration;
        }

        // Remove the faded musics
        bool allFadedRemoved = false;
        while (!allFadedRemoved) {
            bool removed = false;
            foreach (KeyValuePair<string, AudioSource> pair in fadingMusics) {
                if (pair.Value.volume <= 0.0f) {
                    GameObject.Destroy(pair.Value.gameObject);
                    fadingMusics.Remove(pair.Key);
                    removed = true;
                    break;
                }
            }
            if (!removed) {
                allFadedRemoved = true;
            }
        }
    }

}