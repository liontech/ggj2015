﻿using UnityEngine;
using System.Collections;

public class MainCanvas : MonoBehaviour {

    public static MainCanvas Instance { get; private set; }

    private void Awake() {
        if (Instance != null) {
            GameObject.Destroy(gameObject);
            return;
        }
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

}
