### What is this repository for? ###

This repo has been for the development of "Endgame", the 2D slow-paced action RPG made in Unity during the Global Game Jam 2015! Landing page: http://globalgamejam.org/2015/games/endgame-0

![Boss.png](https://bitbucket.org/repo/8eE6Gd/images/1361777413-Boss.png)

You've conquered the Grand Wizard. You've rescued all of Great Burghal. And just now you've destroyed the spawn of ultimate evil, Garak-Sehn the World Eater. And we're not evening mentioning all those stats you've maxed out. So what now? Buy more potions, I suppose. Maybe see if you can find a villager that DOES still have that enticing big exclamation mark above his head. You played the fishing mini-game only 16 times, right? Use the arrow keys to walk around, Space to hit things with your BRAZEN GREATAXE OF FLESH WOUNDS and X to talk to villagers and read signs. Tip of the Day : Visit the store to find many items useful to your Quest!